# package used to work with spreadsheets is openpyxl

import openpyxl

workbook = openpyxl.load_workbook("inventory.xlsx")
product_list = workbook["Sheet1"]

products_per_supplier = {}
total_value_per_supplier = {}


def calculate_products_per_supplier():
    for product_row in range(2, product_list.max_row + 1):
        supplier_name = product_list.cell(product_row, 4).value
        if supplier_name in products_per_supplier:
            current_product_count = products_per_supplier[supplier_name]
            products_per_supplier[supplier_name] = current_product_count + 1
        else:
            products_per_supplier[supplier_name] = 1

    print(products_per_supplier)


calculate_products_per_supplier()


def calculate_total_value_per_supplier():
    for product_row in range(2, product_list.max_row + 1):
        supplier_name = product_list.cell(product_row, 4).value
        inventory = product_list.cell(product_row, 2).value
        price = product_list.cell(product_row, 3).value

        if supplier_name in total_value_per_supplier:
            current_inventory_price = total_value_per_supplier[supplier_name]
            current_product_total = inventory * price
            total_value_per_supplier[supplier_name] = current_inventory_price + current_product_total
        else:
            current_product_total = inventory * price
            total_value_per_supplier[supplier_name] = current_product_total

    print(total_value_per_supplier)


calculate_total_value_per_supplier()
