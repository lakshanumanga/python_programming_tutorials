import datetime

user_input = input("enter your goal with a deadline separated by colon\n")
input_list = user_input.split(":")

goal = input_list[0]
deadline = input_list[1]

deadline_date = datetime.datetime.strptime(deadline, "%d.%m.%Y")
# calculate how many days from now till deadline
today_date = datetime.datetime.today()
date_diff = deadline_date - today_date

print(f"Dear user, time remaining for your goal : {goal} is {date_diff.days} days")