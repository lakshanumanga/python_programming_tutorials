# Dictionary data type in python
my_dictionary = {"days": 20, "units": "hours", "hello": "umanga"}
for item in my_dictionary:
    print(my_dictionary[item])

print(type(my_dictionary))
my_dictionary["message"] = "you rock"
print(my_dictionary)
print(my_dictionary.keys())
print(my_dictionary.items())
print(my_dictionary.values())