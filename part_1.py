# string concatenation
print("20 days are " + str(20 * 24 * 60) + " minutes")
print(f"20 days are {20 * 24 * 60} minutes")

# variables - python is dynamically typed. No need to set the data type
# underscores are a one of many conventions to define variables in python
to_units = 20 * 24 * 60 * 60
name_of_unit = "seconds"
print(f"20 days are {20 * to_units} {name_of_unit}")


# functions
def days_to_units(number_of_days, units):
    print(f"{number_of_days} day are {number_of_days * to_units} {units}")
    print("All good!")


days_to_units(20, "seconds")
days_to_units(50, "hours")
days_to_units(100, "minutes")


# scope
# python cannot understand variables defined inside a function outside of the function
def scope_check():
    my_var = "variable inside function - scope check"
    print(my_var)
    print(name_of_unit)
    # print(number_of_days)


scope_check()

# user input and use it in code
# python has a built-in function to get user input called input
user_input = input("Enter your value\n")
print(f"User has entered {user_input}")


# functions with return values
def function_with_return_value():
    user_input_2 = input("Enter number of days\n")
    return f"User has entered {user_input_2} days"


var_to_store_return_value = function_with_return_value()
print(var_to_store_return_value)


# casting
def function_with_return_value():
    user_input_2 = input("Enter number of days\n")
    return int(user_input_2)


days_to_units(function_with_return_value(), "seconds")
