# file to use in part_9

import random


def generate_random_number_between_one_and_ten():
    return random.randint(1, 9)


def compare_random_number_and_user_input(input_value, generated_number):
    try:
        user_input = int(input_value)
        if user_input == generated_number:
            print("Congratulations, you win!!")
        elif user_input > 10:
            print("Your number is greater than 10. Please enter a number between 1-9")
        elif user_input < 1:
            print("You number is less than 10. Please enter a number between 1-9")
        else:
            print("You lose!\n")
    except ValueError:
        print("You only can enter a number between 1-9. Please check your input.")


input_message_to_user = "Please enter a number between 1-9\nType 'exit' to stop the program\n"
