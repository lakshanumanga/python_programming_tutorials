# working with modules
# importing whole module
import file_two

# importing a single function
from file_two import generate_random_number_between_one_and_ten

# importing module with a new name
import file_two as file

user_value = ""
while user_value != "exit":
    user_value = input(file_two.input_message_to_user)
    file_two.compare_random_number_and_user_input(user_value,file_two.generate_random_number_between_one_and_ten())
