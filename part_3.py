# try/except to better error handling
def validate_and_print():
    try:
        result_in_int = int(result)
        if result_in_int > 0:
            print(result_in_int)
        elif result_in_int == 0:
            print("user has entered 0. Please enter a number > 0")
        else:
            print("you have entered a negative number.")
    # you can leave 'except' for it to be more generic rather than specifying exception
    except ValueError:
        print("your input is not a valid number. Stop trying to blow up my program")


result = ""
while result != "exit":
    result = input("Enter your number\n")
    validate_and_print()
