# looping through code
def validate_and_exit():
    user_input = input_value
    if user_input == "0":
        print("Exiting program.....")
    elif user_input.isdigit():
        print("You have entered a number")
    elif user_input.isalpha():
        print("You have entered a string")


input_value = ""
while input_value != "0":
    input_value = input("Enter what you like\n")
    validate_and_exit()
