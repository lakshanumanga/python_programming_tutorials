# Sets in python
my_list = [20, 40, 50, 80, 20]
my_set = set(my_list)
print(type(my_set))
print(my_set)

for item in my_set:
    print(item)

new_set = {"Umanga", "Lakshan", "Madawala"}
new_set.add("Madawalage")
print(new_set)
new_set.remove("Umanga")
print(new_set)
new_set.pop()
print(new_set)
""" 
no defined order for sets 
no duplicates allowed
"""
